# Nama : Aliif Arief Maulana
# NIM : 21/479029/SV/19418
# Matkul : Praktikum Struktur Data 
# Kelas : PL1AA
# Topik : Min Heap


class MinHeap:
  def __init__(self, n):
    self.n = n
    self.storage = [0]*n
    self.size = 0
  
  # mencari index
  def getParentId(self, idx):
    return int((idx-1)/2)
  def getLeftId(self, idx):
    return int((idx*2)+1)
  def getRightId(self, idx):
    return int((idx*2)+2)

  # cek kanan, kiri, parent
  def hasParent(self, idx):
    return self.getParentId(idx) >= 0
  def hasLeft(self, idx):
    return self.getLeftId(idx) < self.size
  def hasRight(self, idx):
    return self.getRightId(idx) < self.size

  #getValue
  def getParent(self, idx):
    return self.storage[self.getParentId(idx)]
  
  def getLeftChild(self, idx):
    return self.storage[self.getLeftId(idx)]

  def getRightChild(self, idx):
    return self.storage[self.getRightId(idx)]

  # cek apakah full
  def isFull(self):
    return self.size == self.n
  
  def isEmpty(self):
    return self.size == 0
  
  # insert data
  def insert(self, data):
    if self.isFull():
      return
    else:
      self.storage[self.size] = data
      self.size += 1
      self.heapifyup(self.size - 1) #for getting index start from zero
      # print("dimasukkan angka", data)
      # print(self.storage) #track and print the progress
      # print()

  def heapifyup(self, idx):
    if self.getParent(idx) > self.storage[idx]:
      self.swap(self.getParentId(idx), idx)
      self.heapifyup(self.getParentId(idx)) #recursicve

  def heapifydown(self, idx):
    smallidx = idx
    if (self.hasLeft(idx)) and (self.storage[self.getLeftId(idx)] < self.storage[smallidx]):
      smallidx = self.getLeftId(idx)
    if (self.hasRight(idx)) and (self.storage[self.getRightId(idx)] < self.storage[smallidx]):
      smallidx = self.getRightId(idx)
    if smallidx != idx:
      self.swap(idx, smallidx)
      self.heapifydown(smallidx)
  
  def remove(self):
    if not self.isEmpty():
      self.swap(0, self.size - 1)
      self.size = self.size - 1
      self.heapifydown(0)

  # kalau sudah diinsert
  # lakukan swap
  def swap(self,idx1,idx2):
    self.storage[idx1], self.storage[idx2] = self.storage[idx2], self.storage[idx1]
  
  def __str__(self):
    return str(self.storage[:self.size])

NIM =  [4,7,9,0,2,1,8]

minheap = MinHeap(len(NIM))

print("=====insert loop=====")
for data in NIM:
  minheap.insert(data)
  print(minheap)

print("=====remove loop=====")
for i in range(len(NIM)):
  print(minheap)
  minheap.remove()
print(minheap)

print()
print("hasil min heap =", minheap.storage)
print("ukuran min heap =", minheap.size)